package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.service.ILoggerService;
import ru.tsc.apozdnov.tm.exception.system.FileOrDirectoryNotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.*;

public class LoggerService implements ILoggerService {

    @NotNull
    private static final String LOG_DIRECTORY = "log";

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = Paths.get(LOG_DIRECTORY, "commands.xml").toString();

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = Paths.get(LOG_DIRECTORY, "errors.xml").toString();

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = Paths.get(LOG_DIRECTORY, "messages.xml").toString();

    @NotNull
    private final LogManager logManager = LogManager.getLogManager();

    @NotNull
    private final Logger rootLogger = Logger.getLogger("");

    @NotNull
    private final Logger commandsLogger = Logger.getLogger(COMMANDS);

    @NotNull
    private final Logger errorsLogger = Logger.getLogger(ERRORS);

    @NotNull
    private final Logger messagesLogger = Logger.getLogger(MESSAGES);

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        checkLogDirectory();
        init();
        registry(commandsLogger, COMMANDS_FILE, false);
        registry(errorsLogger, ERRORS_FILE, true);
        registry(messagesLogger, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            logManager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (@NotNull final IOException e) {
            rootLogger.severe(e.getMessage());
        }
    }

    private void checkLogDirectory() {
        final Path logsDirectoryPath = Paths.get(LOG_DIRECTORY);
        if (Files.exists(logsDirectoryPath)) {
            if (!Files.isDirectory(logsDirectoryPath) || !Files.isWritable(logsDirectoryPath)) {
                throw new FileOrDirectoryNotFoundException();
            }
        } else {
            try {
                Files.createDirectory(logsDirectoryPath);
            } catch (IOException e) {
                rootLogger.severe(e.getMessage());
            }
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String logFileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(logFileName));
        } catch (final IOException e) {
            rootLogger.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler consoleHandler = new ConsoleHandler();
        @NotNull final Formatter formatter = new Formatter() {
            @Override
            public String format(final LogRecord record) {
                return record.getMessage() + "\n";
            }
        };
        consoleHandler.setFormatter(formatter);
        return consoleHandler;
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messagesLogger.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        messagesLogger.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        commandsLogger.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        errorsLogger.log(Level.SEVERE, e.getMessage(), e);
    }

}
