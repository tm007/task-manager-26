package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.ITaskService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.DescriptionEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.exception.field.NameEmptyException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.*;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull final String name, @NotNull final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = Optional.ofNullable(create(name, description)).orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        task.setUserId(userId);
        return task;
    }

    @Override
    public @NotNull Task remove(@NotNull String userId, final @NotNull Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        return repository.remove(task);
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull String userId, final @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task updateById(@NotNull String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = Optional.ofNullable(findOneById(id)).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(@NotNull String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Task task = Optional.ofNullable(findOneByIndex(index)).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@NotNull String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = Optional.ofNullable(findOneById(id)).orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(@NotNull String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = Optional.ofNullable(findOneByIndex(index)).orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}
