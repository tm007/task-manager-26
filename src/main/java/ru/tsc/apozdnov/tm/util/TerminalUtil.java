package ru.tsc.apozdnov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static Date nextDate() {
        final String value = nextLine();
        return DateUtil.toDate(value);
    }

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        final String val = nextLine();
        return Integer.parseInt(val);
    }

}
