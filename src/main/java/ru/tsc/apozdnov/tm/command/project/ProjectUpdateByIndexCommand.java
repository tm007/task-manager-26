package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** UPDATE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        @NotNull final Integer id = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        serviceLocator.getProjectService().updateByIndex(userId, id, name, description);
    }

}
