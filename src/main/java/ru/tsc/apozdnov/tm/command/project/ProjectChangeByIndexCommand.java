package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-status-by-index";

    @NotNull
    public static final String DESCRIPTION = "Change project status by index.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** CHANGE PROJECT STATUS BY INDEX ****");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final String userId = getUserId();
        serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
    }

}
